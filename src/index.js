import "./main.css"
import { Elm } from "./Main.elm"

const SETTINGS_KEY = "settings"
const getSettings = () => JSON.parse(localStorage.getItem(SETTINGS_KEY))
const setSettings = (s) => localStorage.setItem(SETTINGS_KEY, JSON.stringify(s))

var app = Elm.Main.init({
  node: document.getElementById("root"),
})

if (!app.ports.getSettings || !app.ports.onSettingsGet) {
  console.warn("Unable to initialize settings getter.")
} else {
  app.ports.getSettings.subscribe(() => {
    app.ports.onSettingsGet.send(getSettings())
  })
}

if (!app.ports.saveSettings) {
  console.warn("Unable to initialize settings setter.")
} else {
  app.ports.saveSettings.subscribe((settings) => {
    setSettings(settings)
  })
}

if (!app.ports.initClient || !app.ports.onClientInit) {
  console.warn("Unable to initialize client port.")
} else {
  app.ports.initClient.subscribe((v) => {
    stopTransmission(intervalId)

    let data = { role: 0 }
    if (v == 1) {
      data = { role: 1, technologies: ["VNC", "WebRTC"] }
    } else if (v == 2) {
      data = { role: 1, technologies: ["WebRTC", "VNC"] }
    }

    app.ports.onClientInit.send(data)
  })
}

if (
  !app.ports.start ||
  !app.ports.onStart ||
  !app.ports.stop ||
  !app.ports.pause ||
  !app.ports.resume ||
  !app.ports.panel
) {
  console.warn("Unable to initialize play actions.")
} else {
  let started = false
  let pause = false

  app.ports.start.subscribe((t) => {
    const settings = getSettings() || {}
    let result = { state: "error" }
    if (!settings.disableWebRTC && t == "WebRTC") {
      result = { state: "ok", mode: t }
    } else if (!settings.disableVNC && t == "VNC") {
      result = { state: "ok", mode: t, mobile: !!settings.mobile }
    }

    app.ports.onStart.send(result)

    if (result.state == "ok") {
      started = true
      pause = false

      intervalId = startTransmission(t, intervalId)
    }
  })

  app.ports.stop.subscribe(() => {
    started = pause = false
    stopTransmission(intervalId)
  })

  app.ports.pause.subscribe(() => {
    pause = true
  })

  app.ports.resume.subscribe(() => {
    pause = false
  })

  app.ports.panel.subscribe((p) => {
    console.log("panel selected:", p)
  })
}

let intervalId = false

function startTransmission(t, iid) {
  const transmissionData = () => {
    const data =
      t == "WebRTC"
        ? { screens: ["screen1", "screen2", "screen3"] }
        : {
            windows: ["win1", "win2", "win3"],
            screens: ["screen1", "screen2", "screen3"],
          }

    app.ports.onTransmissionData && app.ports.onTransmissionData.send(data)
  }

  transmissionData()

  const settings = getSettings() || {}

  iid && clearInterval(iid)
  return !settings.mobile && setInterval(transmissionData, 2000)
}

function stopTransmission(iid) {
  iid && clearInterval(iid)
}
