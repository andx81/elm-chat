port module Interop exposing
    ( getSettings
    , initClient
    , onClientInit
    , onSettingsGet
    , onStart
    , onTransmissionData
    , panel
    , pause
    , resume
    , saveSettings
    , start
    , stop
    )

import Json.Decode as Decode


port initClient : Int -> Cmd msg


port onClientInit : (Decode.Value -> msg) -> Sub msg


port saveSettings : Decode.Value -> Cmd msg


port getSettings : () -> Cmd msg


port onSettingsGet : (Decode.Value -> msg) -> Sub msg


port start : String -> Cmd msg


port onStart : (Decode.Value -> msg) -> Sub msg


port panel : String -> Cmd msg


port stop : () -> Cmd msg


port pause : () -> Cmd msg


port resume : () -> Cmd msg


port onTransmissionData : (Decode.Value -> msg) -> Sub msg
