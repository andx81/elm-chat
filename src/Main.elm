module Main exposing (..)

import Browser
import Client
import Html exposing (Html, button, div, input, label, span, text)
import Html.Attributes exposing (checked, for, id, style, type_)
import Html.Events exposing (onClick)
import Interop
import Json.Decode as Decode
import Json.Encode as Encode



---- SETTINGS ----


type alias Settings =
    { disableVNC : Bool
    , disableWebRTC : Bool
    , mobile : Bool
    }


updateSettings : (Settings -> Settings) -> Settings -> Settings
updateSettings f settings =
    f settings


defaultSettings : Settings
defaultSettings =
    { disableVNC = False
    , disableWebRTC = False
    , mobile = False
    }


settingsDecoder : Decode.Decoder Settings
settingsDecoder =
    Decode.map3 Settings
        (Decode.field "disableVNC" Decode.bool)
        (Decode.field "disableWebRTC" Decode.bool)
        (Decode.field "mobile" Decode.bool)


settingsEncoder : Settings -> Encode.Value
settingsEncoder s =
    Encode.object
        [ ( "disableVNC", Encode.bool s.disableVNC )
        , ( "disableWebRTC", Encode.bool s.disableWebRTC )
        , ( "mobile", Encode.bool s.mobile )
        ]



---- MODEL ----


type alias Model =
    { client : Client.Model
    , settings : Settings
    }


init : ( Model, Cmd Msg )
init =
    let
        ( client, clientCmd ) =
            Client.init
    in
    ( { client = client
      , settings = defaultSettings
      }
    , Cmd.batch [ Cmd.map ClientMsg clientCmd, Interop.getSettings () ]
    )



---- UPDATE ----


type Msg
    = NoOp
    | OnSettingsGet Decode.Value
    | ToggleVNC
    | ToggleWebRTC
    | ToggleMobile
    | InitPresenter1
    | InitPresenter2
    | InitParticipant
    | ClientMsg Client.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update cmd model =
    case cmd of
        NoOp ->
            ( model, Cmd.none )

        OnSettingsGet v ->
            ( { model
                | settings =
                    Decode.decodeValue settingsDecoder v |> Result.withDefault defaultSettings
              }
            , Cmd.none
            )

        ToggleVNC ->
            let
                newSettings =
                    updateSettings (\s -> { s | disableVNC = not s.disableVNC }) model.settings
            in
            ( { model | settings = newSettings }, Interop.saveSettings <| settingsEncoder newSettings )

        ToggleWebRTC ->
            let
                newSettings =
                    updateSettings (\s -> { s | disableWebRTC = not s.disableWebRTC }) model.settings
            in
            ( { model | settings = newSettings }, Interop.saveSettings <| settingsEncoder newSettings )

        ToggleMobile ->
            let
                newSettings =
                    updateSettings (\s -> { s | mobile = not s.mobile }) model.settings
            in
            ( { model | settings = newSettings }, Interop.saveSettings <| settingsEncoder newSettings )

        InitPresenter1 ->
            ( model, Interop.initClient 1 )

        InitPresenter2 ->
            ( model, Interop.initClient 2 )

        InitParticipant ->
            ( model, Interop.initClient 3 )

        ClientMsg msg ->
            let
                ( client, clientCmd ) =
                    Client.update msg model.client
            in
            ( { model | client = client }, Cmd.map ClientMsg clientCmd )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Sub.map ClientMsg <| Client.subscriptions model.client
        , Interop.onSettingsGet OnSettingsGet
        ]



---- VIEW ----


view : Model -> Html Msg
view model =
    div
        [ style "height" "100vh", style "display" "flex", style "flex-direction" "column" ]
        [ viewSettings model
        , Html.map ClientMsg <| Client.view model.client
        ]


viewSettings : Model -> Html Msg
viewSettings model =
    div [ style "padding" "8px", style "background-color" "#aaa" ]
        [ button [ style "margin" "0 20px", onClick InitPresenter1 ] [ text "Init as Presenter [VNC, WebRTC]" ]
        , button [ style "margin" "0 20px", onClick InitPresenter2 ] [ text "Init as Presenter [WebRTC, VNC]" ]
        , button [ style "margin" "0 20px", onClick InitParticipant ] [ text "Init as Participant" ]
        , span [ style "margin" "0 8px" ]
            [ input [ id "disableVNC", type_ "checkbox", checked model.settings.disableVNC, onClick ToggleVNC ] []
            , label [ for "disableVNC" ] [ text "disable VNC" ]
            ]
        , span [ style "margin" "0 8px" ]
            [ input [ id "disableWebRTC", type_ "checkbox", checked model.settings.disableWebRTC, onClick ToggleWebRTC ] []
            , label [ for "disableWebRTC" ] [ text "disable WebRTC" ]
            ]
        , span [ style "margin" "0 8px" ]
            [ input [ id "mobile", type_ "checkbox", checked model.settings.mobile, onClick ToggleMobile ] []
            , label [ for "mobile" ] [ text "mobile" ]
            ]
        ]



---- PROGRAM ----


main : Program () Model Msg
main =
    Browser.element
        { view = view
        , init = \_ -> init
        , update = update
        , subscriptions = subscriptions
        }
