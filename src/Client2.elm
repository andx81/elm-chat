module Client2 exposing (..)

import Html exposing (Html, div, span, text)


type RemoteData e a
    = NotAsked
    | Loading
    | Failure e
    | Success a


type Client
    = Presenter (List Technology)
    | Participant


type Technology
    = VNC
    | WebRTC


{-| It would be better to make two different clients, one for the Presenter and the other for the Participant.
But for our example, for simplicity, i added Client to the Model
-}
type alias Model =
    { client : Maybe Client
    , session : RemoteData TransmissionError Transmission
    }


{-| This type holds information about specific transmission modes (VNC mobile/desktop
and WebRTC) and corresponding data required for each mode.
This allows us to select from the only available option when sharing.
Updates coming from the server will update screens/windows on a transmission
with a corresponding mode
-}
type Transmission
    = VNCMobileTransmission (SharingState SharedVNCMobile)
    | VNCDesktopTransmission VNCDesktopData (SharingState SharedVNCDesktop)
    | WebRTCTransmission WebRTCData (SharingState SharedWebRTC)


type TransmissionError
    = StartSessionError
    | OtherError String


type VNCDesktopData
    = VNCDesktopData (List Window) (List Screen)


type WebRTCData
    = WebRTCData (List Screen)


type SharingState a
    = Idle
    | Sharing a
    | Paused a


type SharedVNCMobile
    = SharedVNCMobile


type SharedVNCDesktop
    = SharedVNCDesktopScreen Screen
    | SharedVNCDesktopWindow Window


type SharedWebRTC
    = SharedWebRTC Screen


type alias WithTitle r =
    { r | title : String }


type ScreenId
    = ScreenId String


type alias Screen =
    WithTitle { id : ScreenId }


type WindowId
    = WindowId String


type alias Window =
    WithTitle { id : WindowId }


init : ( Model, Cmd Msg )
init =
    ( { client = Nothing
      , session = NotAsked
      }
    , Cmd.none
    )



---- Update
-------------------------------------------------------------------------------


type Msg
    = NoOp
    | Init Client
    | StartTransmission Technology
    | GotStartTransmissionResponse (RemoteData TransmissionError Transmission)
    | GotVNCDesktopData VNCDesktopData


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        Init client ->
            ( { model | client = Just client }, Cmd.none )

        StartTransmission _ ->
            ( { model | session = Loading }, Cmd.none )

        GotStartTransmissionResponse response ->
            ( { model | session = response }, Cmd.none )

        GotVNCDesktopData data ->
            -- Since we cannot guarantee that the correct data will come from the server,
            -- we need to make sure that the received data corresponds to the current transmition mode.
            --
            -- For example, if the current mode is VNC with mobile mode, but the server can still send us data as if it were VNC without mobile mode.
            -- In this case, we need to ignore this data.
            case model.session of
                Success (VNCDesktopTransmission _ s) ->
                    ( { model | session = Success (VNCDesktopTransmission data s) }, Cmd.none )

                _ ->
                    ( model, Cmd.none )



-- View
-------------------------------------------------------------------------------


view : Model -> Html Msg
view model =
    case model.client of
        Just (Presenter _) ->
            div []
                [ case model.session of
                    NotAsked ->
                        text "NotAsked"

                    Loading ->
                        text "Loading session..."

                    Failure StartSessionError ->
                        text "starting session failed"

                    Failure (OtherError err) ->
                        text <| "something went wrong: " ++ err

                    Success t ->
                        viewTransmission t
                , text "render Start/Stop/Pause/Resume buttons"
                ]

        Just Participant ->
            text "view of someone's screen"

        Nothing ->
            text "client not initialized"


viewTransmission : Transmission -> Html Msg
viewTransmission transmission =
    case transmission of
        VNCMobileTransmission s ->
            viewSharing s shareVNCMobile

        VNCDesktopTransmission _ s ->
            div []
                [ viewSharing s shareVNCDesktop
                , text "render Windows and Screens selector"
                ]

        WebRTCTransmission _ s ->
            div []
                [ viewSharing s shareWebRTC
                , text "render Screens selector"
                ]


viewSharing : SharingState a -> (a -> Html Msg) -> Html Msg
viewSharing s share =
    case s of
        Idle ->
            span [] [ text "idle" ]

        Sharing x ->
            span [] [ text "sharing: ", share x ]

        Paused x ->
            span [] [ text "paused: ", share x ]


shareVNCMobile : SharedVNCMobile -> Html Msg
shareVNCMobile _ =
    text "Mobile Display"


shareVNCDesktop : SharedVNCDesktop -> Html Msg
shareVNCDesktop x =
    case x of
        SharedVNCDesktopScreen screen ->
            renderScreen screen

        SharedVNCDesktopWindow window ->
            renderWindow window


shareWebRTC : SharedWebRTC -> Html Msg
shareWebRTC (SharedWebRTC screen) =
    renderScreen screen


renderScreen : Screen -> Html Msg
renderScreen screen =
    text screen.title


renderWindow : Window -> Html Msg
renderWindow window =
    text window.title
