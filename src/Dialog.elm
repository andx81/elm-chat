module Dialog exposing (modal)

import Html exposing (Attribute, Html, div, span, text)
import Html.Attributes exposing (style)
import Html.Events exposing (onClick)



-- Types
-------------------------------------------------------------------------------


type alias Config msg =
    { onClose : msg
    , visible : Bool
    , content : Html msg
    }



-- Dialog view
-------------------------------------------------------------------------------


modal : Config msg -> Html msg
modal cfg =
    if cfg.visible then
        overlay [ style "background-color" "rgba(0,0,0, 0.1)" ] <|
            div
                [ style "background-color" "#fff"
                , style "border" "1px solid #999"
                ]
                [ div [ style "padding" "8px", style "text-align" "right" ]
                    [ span [ style "cursor" "pointer", onClick cfg.onClose ] [ text "[X]" ]
                    ]
                , div [ style "padding" "8px" ] [ cfg.content ]
                ]

    else
        text ""


overlay : List (Attribute msg) -> Html msg -> Html msg
overlay attrs inner =
    div
        ([ style "position" "fixed"
         , style "top" "0"
         , style "left" "0"
         , style "width" "100%"
         , style "height" "100%"
         , style "z-index" "100"
         , style "display" "flex"
         , style "justify-content" "center"
         , style "align-items" "center"
         ]
            ++ attrs
        )
        [ inner ]
