module Client exposing (Model, Msg, init, subscriptions, update, view)

import Dialog
import Html exposing (Html, button, div, text)
import Html.Attributes exposing (disabled, style)
import Html.Events exposing (onClick)
import Interop
import Json.Decode as Decode
import Task



---- Model
-------------------------------------------------------------------------------
-- If we need to guarantee uniqueness of presenter technologies - we can verify this at compile time by making it a Set of technologies (where each element is unique).
-- Alternatively - we can write value-level helper utilities to make sure that this list is ALWAYS unique (when we get the data) and cover it with unit tests


type Client
    = Presenter (List Technology) -- Maybe better use Set?
    | Participant


type Technology
    = VNC
    | WebRTC


type Transmission
    = TVNC
    | TVNCMobile
    | TWebRTC
    | TError TransmissionError


type TransmissionError
    = ServerError
    | DecodeError


type TransmissionData
    = VNCData (List Window) (List Screen)
    | WebRTCData (List Screen)


type Window
    = Window String


type Screen
    = Screen String


type Panel
    = WPanel Window
    | SPanel Screen
    | MPanel



-- Idle | Starting | Started
-- TransmissionStatus


type Status
    = Stopted
    | Starting
    | Started Transmission



-- | Started( Result String Transmission)


type alias Model =
    { client : Maybe Client
    , technology : Maybe Technology
    , status : Status
    , pause : Bool
    , transmissionData : Maybe TransmissionData
    , activePanel : Maybe Panel

    -- UI
    , sharingPopupOpen : Bool
    , currentTab : SharingPopupTab
    }


type SharingPopupTab
    = ScreensTab
    | WindowsTab


init : ( Model, Cmd Msg )
init =
    ( { client = Nothing
      , technology = Nothing
      , status = Stopted
      , pause = False
      , transmissionData = Nothing
      , activePanel = Nothing

      -- UI
      , sharingPopupOpen = False
      , currentTab = ScreensTab
      }
    , Cmd.none
    )



---- Update
-------------------------------------------------------------------------------


type Msg
    = NoOp
    | Init Decode.Value
    | ToggleStart
    | TogglePause
    | Start
    | StartResponse Decode.Value
    | TransmissionResponse Decode.Value
    | OpenSharingPopup SharingPopupTab
    | HideSharingPopup
    | SelectScreen Screen
    | SelectWindow Window


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        Init v ->
            ( { model
                | status = Stopted
                , client = Decode.decodeValue clientDecoder v |> Result.toMaybe
                , transmissionData = Nothing
                , technology = Nothing
              }
            , Cmd.none
            )

        ToggleStart ->
            if model.status == Stopted then
                ( { model | status = Starting, pause = False }
                , (Task.perform identity << Task.succeed) Start
                )

            else
                ( { model | status = Stopted, pause = False, transmissionData = Nothing, technology = Nothing }
                , Interop.stop ()
                )

        TogglePause ->
            let
                pause =
                    not model.pause
            in
            ( { model | pause = pause }
            , if pause then
                Interop.pause ()

              else
                Interop.resume ()
            )

        Start ->
            case model.client of
                Just (Presenter ts) ->
                    case fetchTechnology model.technology ts of
                        Just technology ->
                            ( { model | status = Starting, pause = False, technology = Just technology }, Interop.start <| fromTechnology technology )

                        Nothing ->
                            ( model, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        StartResponse v ->
            let
                transmission =
                    case Decode.decodeValue transmissionDecoder v of
                        Ok r ->
                            r

                        Err _ ->
                            TError DecodeError

                activePanel =
                    if transmission == TVNCMobile then
                        Just MPanel

                    else
                        model.activePanel
            in
            case transmission of
                TError ServerError ->
                    ( { model | status = Started transmission }, (Task.perform identity << Task.succeed) Start )

                _ ->
                    ( { model | status = Started transmission, activePanel = activePanel }, Cmd.none )

        TransmissionResponse v ->
            let
                transmissionData =
                    Maybe.withDefault Nothing <|
                        case model.status of
                            Started t ->
                                Decode.decodeValue (transmissionDataDecoder t) v |> Result.toMaybe

                            _ ->
                                Nothing
            in
            ( { model | transmissionData = transmissionData }, Cmd.none )

        OpenSharingPopup tab ->
            ( { model | sharingPopupOpen = True, currentTab = tab }, Cmd.none )

        HideSharingPopup ->
            ( { model | sharingPopupOpen = False }, Cmd.none )

        SelectScreen (Screen s) ->
            ( { model | activePanel = Just <| SPanel (Screen s) }
            , Cmd.batch
                [ Interop.panel s
                , (Task.perform identity << Task.succeed) HideSharingPopup
                ]
            )

        SelectWindow (Window w) ->
            ( { model | activePanel = Just <| WPanel (Window w) }
            , Cmd.batch
                [ Interop.panel w
                , (Task.perform identity << Task.succeed) HideSharingPopup
                ]
            )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch
        [ Interop.onClientInit Init
        , Interop.onStart StartResponse
        , Interop.onTransmissionData TransmissionResponse
        ]


fetchTechnology : Maybe Technology -> List Technology -> Maybe Technology
fetchTechnology mt ts =
    let
        findIndex : Int -> (a -> Bool) -> List a -> Maybe Int
        findIndex index predicate list =
            case list of
                [] ->
                    Nothing

                x :: xs ->
                    if predicate x then
                        Just index

                    else
                        findIndex (index + 1) predicate xs
    in
    case mt of
        Just t ->
            List.drop ((+) 1 <| Maybe.withDefault -1 <| findIndex 0 ((==) t) ts) ts |> List.head

        Nothing ->
            List.head ts



-- View
-------------------------------------------------------------------------------


view : Model -> Html Msg
view model =
    div
        [ style "flex" "1"
        , style "display" "flex"
        , style "justify-content" "center"
        , style "align-items" "center"
        , style "background-color" "#ddd"
        ]
        [ case model.client of
            Just (Presenter _) ->
                presenterView model

            Just Participant ->
                participantView model

            Nothing ->
                text ""
        ]


participantView : Model -> Html Msg
participantView _ =
    text "looking at someone's screen..."


presenterView : Model -> Html Msg
presenterView model =
    let
        pauseDisable =
            case model.status of
                Started _ ->
                    False

                _ ->
                    True

        renderPanel : Panel -> Transmission -> Bool -> Html msg
        renderPanel panel t pause =
            let
                panelText =
                    case panel of
                        WPanel (Window w) ->
                            "window: " ++ w ++ ""

                        SPanel (Screen s) ->
                            "screen: " ++ s ++ ""

                        MPanel ->
                            "Mobile Display"

                transmissionText =
                    case t of
                        TVNC ->
                            "technology: VNC"

                        TVNCMobile ->
                            "technology: VNC"

                        TWebRTC ->
                            "technology: WebRTC"

                        TError _ ->
                            "error"

                pauseText =
                    if pause then
                        "PAUSE"

                    else
                        ""
            in
            text <| panelText ++ "; " ++ transmissionText ++ "; " ++ pauseText
    in
    div
        [ style "display" "flex"
        , style "width" "100%"
        , style "height" "100%"
        , style "flex-direction" "column"
        , style "justify-content" "center"
        , style "align-items" "center"
        ]
        [ div []
            [ case model.status of
                Stopted ->
                    text "press Start"

                Starting ->
                    text "starting transmition..."

                Started (TError ServerError) ->
                    text "server error"

                Started (TError DecodeError) ->
                    text "decode server data failed"

                Started t ->
                    case model.activePanel of
                        Just p ->
                            renderPanel p t model.pause

                        Nothing ->
                            text "select window or screen"
            ]
        , div
            [ style "position" "absolute"
            , style "bottom" "20px"
            , style "display" "flex"
            ]
            [ playButton (model.status /= Stopted) False
            , pauseButton model.pause pauseDisable
            , sharingView model
            ]
        ]


sharingView : Model -> Html Msg
sharingView model =
    let
        tab : SharingPopupTab -> Html Msg
        tab t =
            div
                ([ style "flex" "1", style "padding" "4px" ]
                    ++ (if model.currentTab == t then
                            [ style "background-color" "#aaa" ]

                        else
                            [ style "cursor" "pointer", onClick (OpenSharingPopup t) ]
                       )
                )
                [ text <|
                    case t of
                        ScreensTab ->
                            "Screens"

                        WindowsTab ->
                            "Windows"
                ]

        window : Window -> Html Msg
        window (Window title) =
            div [ style "margin" "8px 0", style "cursor" "pointer", onClick <| SelectWindow (Window title) ] [ text title ]

        screen : Screen -> Html Msg
        screen (Screen id) =
            div [ style "margin" "8px 0", style "cursor" "pointer", onClick <| SelectScreen (Screen id) ] [ text id ]
    in
    case model.transmissionData of
        Just (VNCData ws ss) ->
            div []
                [ Dialog.modal
                    { onClose = HideSharingPopup
                    , visible = model.sharingPopupOpen
                    , content =
                        div [ style "width" "200px" ]
                            [ div [ style "display" "flex" ] [ tab WindowsTab, tab ScreensTab ]
                            , div [] <|
                                case model.currentTab of
                                    WindowsTab ->
                                        List.map window ws

                                    ScreensTab ->
                                        List.map screen ss
                            ]
                    }
                , div []
                    [ button [ style "margin" "0 6px", onClick (OpenSharingPopup WindowsTab) ] [ text "Select Window" ]
                    , button [ style "margin" "0 6px", onClick (OpenSharingPopup ScreensTab) ] [ text "Select Screen" ]
                    ]
                ]

        Just (WebRTCData ss) ->
            div []
                [ Dialog.modal
                    { onClose = HideSharingPopup
                    , visible = model.sharingPopupOpen
                    , content =
                        div [ style "width" "200px" ]
                            [ div [] <| List.map screen ss
                            ]
                    }
                , div []
                    [ button [ style "margin" "0 6px", onClick (OpenSharingPopup ScreensTab) ] [ text "Select Screen" ]
                    ]
                ]

        Nothing ->
            text ""


playButton : Bool -> Bool -> Html Msg
playButton s d =
    button [ style "margin" "0 6px", disabled d, onClick ToggleStart ]
        [ (if s then
            "Stop"

           else
            "Start"
          )
            |> text
        ]


pauseButton : Bool -> Bool -> Html Msg
pauseButton p d =
    button [ style "margin" "0 6px", disabled d, onClick TogglePause ]
        [ (if p then
            "Resume"

           else
            "Pause"
          )
            |> text
        ]



---- DECODERS / ENCODERS
-------------------------------------------------------------------------------


clientDecoder : Decode.Decoder Client
clientDecoder =
    Decode.field "role" Decode.int
        |> Decode.andThen
            (\role ->
                if role == 0 then
                    Decode.succeed <| Participant

                else
                    Decode.map (\ts -> Presenter ts)
                        (Decode.field "technologies" (Decode.list technologyDecoder))
            )


technologyDecoder : Decode.Decoder Technology
technologyDecoder =
    Decode.string
        |> Decode.andThen
            (\t ->
                case t of
                    "VNC" ->
                        Decode.succeed VNC

                    "WebRTC" ->
                        Decode.succeed WebRTC

                    _ ->
                        Decode.fail "decode technology failed"
            )


fromTechnology : Technology -> String
fromTechnology t =
    case t of
        VNC ->
            "VNC"

        WebRTC ->
            "WebRTC"


transmissionDecoder : Decode.Decoder Transmission
transmissionDecoder =
    Decode.field "state" Decode.string
        |> Decode.andThen
            (\role ->
                if role == "ok" then
                    Decode.map2
                        (\t m ->
                            case ( t, m ) of
                                ( WebRTC, _ ) ->
                                    TWebRTC

                                ( VNC, Just True ) ->
                                    TVNCMobile

                                ( VNC, _ ) ->
                                    TVNC
                        )
                        (Decode.field "mode" technologyDecoder)
                        (Decode.maybe (Decode.field "mobile" Decode.bool))

                else
                    Decode.succeed (TError ServerError)
            )


transmissionDataDecoder : Transmission -> Decode.Decoder (Maybe TransmissionData)
transmissionDataDecoder t =
    case t of
        TWebRTC ->
            Decode.map (Just << WebRTCData << List.map Screen)
                (Decode.field "screens" (Decode.list Decode.string))

        TVNC ->
            Decode.map2 (\ws ss -> Just <| VNCData (List.map Window ws) (List.map Screen ss))
                (Decode.field "windows" (Decode.list Decode.string))
                (Decode.field "screens" (Decode.list Decode.string))

        _ ->
            Decode.succeed Nothing
